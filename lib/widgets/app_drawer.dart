import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Drawer(
      child: Column(
        children: [
          // AppBar(
          //   title: Text('MENU'),
          //   automaticallyImplyLeading: false,
          // ),
          // Divider(),

          UserAccountsDrawerHeader(
            accountEmail: Text('Ram@Ram.com'),
            accountName: Text('Prajesh'),
            currentAccountPicture: CircleAvatar(
              child: FlutterLogo(
                textColor: Theme.of(context).accentColor,
              ),
            ),
          ),

          ListTile(
            leading: Icon(Icons.volunteer_activism),
            title: Text('My Giveaway'),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.connect_without_contact_outlined),
            title: Text('Claimed'),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.save),
            title: Text('Saved'),
            onTap: () {},
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Log Out'),
            onTap: () {
              Navigator.of(context).pop();

              // Provider.of<Auth>(context, listen: false).logOut();
            },
          ),
          Spacer(),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'All Rights Reserved by daVariable Innovations',
              style: TextStyle(
                fontSize: 8,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: deviceSize.height * 0.04,
              width: deviceSize.width * 0.04,
              child: FittedBox(
                fit: BoxFit.cover,
                child: Image(
                  image: AssetImage('assets/images/dvlogo.jpg'),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

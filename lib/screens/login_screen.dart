import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';
import '../models/http_exceptions.dart';

enum AuthMode { SignUp, Login }

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AuthMode _authMode = AuthMode.Login;

  Map<String, String> _authData = {
    // 'username': '',
    'email': '',
    'password': '',
  };

  var _isLoading = false;

  TextEditingController _passwordController = TextEditingController();
  TextEditingController _emailController = TextEditingController();

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.SignUp;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('An Error Occured!'),
        content: Text(message),
        // backgroundColor: Colors.black12,
        actions: [
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });

    try {
      if (_authMode == AuthMode.Login) {
        await Provider.of<Auth>(context, listen: false).logIn(
          _authData['email'],
          _authData['password'],
        );
        Navigator.of(context).pushReplacementNamed('/main_screen');
      } else {
        await Provider.of<Auth>(context, listen: false).signUp(
          // _authData['username'],
          _authData['email'],
          _authData['password'],
        );
        showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Succesfully Signed Up'),
            content: Text('Please Login To Continue'),
            // backgroundColor: Colors.black12,
            actions: [
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    _authMode = AuthMode.Login;
                    _passwordController.clear();
                    _emailController.clear();
                  });
                },
              )
            ],
          ),
        );
      }
    } on HttpException catch (error) {
      var errorMessage = 'Authentication Failed!';

      if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'This email already exists';
      } else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = ' Invalid Email';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = ' This password is too weak';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = ' Couldnt find the user with that email';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = ' Invalid Password.';
      }
      _showErrorDialog(errorMessage);
    } catch (error) {
      const errorMessage = 'Could Not Aunthenticate. Please Try Again Later!';
      _showErrorDialog(errorMessage);
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: Center(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  blurRadius: 1,
                  offset: Offset(3, 3), // Shadow position
                ),
              ],
            ),
            height: _authMode == AuthMode.SignUp
                ? deviceSize.height * 0.78
                : deviceSize.height * 0.65,
            width: deviceSize.width * 0.90,
            child: Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: ListView(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: deviceSize.height * 0.15,
                          width: deviceSize.width * 0.15,
                          child: FittedBox(
                            fit: BoxFit.cover,
                            child: Image(
                              image: AssetImage('assets/images/foodlogo.png'),
                            ),
                          ),
                        ),
                      ],
                    ),

                    // Text(
                    //   'Login Page',
                    //   style: TextStyle(
                    //       color: Theme.of(context).primaryColor,
                    //       fontWeight: FontWeight.w500,
                    //       fontSize: 30),
                    // )),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              if (_authMode == AuthMode.SignUp)
                                Container(
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: TextFormField(
                                    enabled: _authMode == AuthMode.SignUp,
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter a username';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      _authData['username'] = value;
                                    },
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'User Name',
                                    ),
                                  ),
                                ),
                              Container(
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: TextFormField(
                                  controller: _emailController,
                                  validator: (value) {
                                    if (value.isEmpty ||
                                        !value.contains('@') ||
                                        !value.contains('.com')) {
                                      return 'Please enter a valid email';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    _authData['email'] = value;
                                  },
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Email',
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: TextFormField(
                                  obscureText: true,
                                  controller: _passwordController,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Password',
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty || value.length <= 6) {
                                      return 'Enter a longer password';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    _authData['password'] = value;
                                  },
                                ),
                              ),
                              if (_authMode == AuthMode.SignUp)
                                Container(
                                  padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                                  child: TextFormField(
                                    enabled: _authMode == AuthMode.SignUp,
                                    obscureText: true,
                                    // controller: _passwordController,
                                    validator: _authMode == AuthMode.SignUp
                                        ? (value) {
                                            if (value !=
                                                _passwordController.text) {
                                              return 'Password do not match';
                                            }
                                            return null;
                                          }
                                        : null,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Confirm Password ',
                                    ),
                                  ),
                                ),
                              // SizedBox(
                              //   height: 07,
                              // ),
                              Visibility(
                                visible:
                                    _authMode == AuthMode.Login ? true : false,
                                child: FlatButton(
                                  onPressed: () {
                                    //forgot password screen
                                  },
                                  textColor: Theme.of(context).accentColor,
                                  child: Text('Forgot Password'),
                                ),
                              ),
                              Container(
                                // height: 50,
                                // width: 250,
                                child: _isLoading
                                    ? CircularProgressIndicator()
                                    : ElevatedButton(
                                        style: ButtonStyle(),
                                        child: _authMode == AuthMode.Login
                                            ? Text('Login')
                                            : Text('Sign Up'),
                                        onPressed: _submit,
                                      ),
                              ),
                              // SizedBox(
                              //   height: 03,
                              // ),
                              Container(
                                child: Row(
                                  children: [
                                    _authMode == AuthMode.Login
                                        ? Text('Does not have an account?')
                                        : Text('Already have an account?'),
                                    FlatButton(
                                      textColor: Theme.of(context).accentColor,
                                      child: _authMode == AuthMode.Login
                                          ? Text(
                                              'Sign Up',
                                              style: TextStyle(fontSize: 20),
                                            )
                                          : Text(
                                              'Login',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                      onPressed: _switchAuthMode,
                                    )
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.center,
                                ),
                              ),
                              Divider(
                                color: Colors.black,
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text('Powered by daVariables'),
                                  Container(
                                    height: deviceSize.height * 0.04,
                                    width: deviceSize.width * 0.04,
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/dvlogo.gif'),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ));
  }
}

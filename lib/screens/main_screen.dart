import 'package:flutter/material.dart';

import '../widgets/app_drawer.dart';

enum FilterOptions {
  Favorite,
  All,
}

class MainScreen extends StatefulWidget {
  static const routeName = '/main_screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var _showOnlyFavorite = false;

  void _startAddNewTransaction(BuildContext ctx) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      context: ctx,
      builder: (_) {
        return SingleChildScrollView(
          child: Column(
            children: [
              GestureDetector(
                onTap: () {},
                child: Card(
                  margin: EdgeInsets.symmetric(
                    horizontal: 3,
                    vertical: 4,
                  ),
                  child: Container(
                    height: 100,
                    alignment: Alignment.center,
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 50,
                        child: Padding(
                          padding: EdgeInsets.all(5),
                          child: FittedBox(
                            child: Icon(
                              Icons.restaurant_menu_rounded,
                              color: Theme.of(context).primaryColor,
                              size: 90,
                            ),
                          ),
                        ),
                      ),
                      title: Text(
                        'Food',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text('Give what you dont need.'),
                    ),
                  ),
                ),
                behavior: HitTestBehavior.opaque,
              ),
              GestureDetector(
                onTap: () {},
                child: Card(
                  margin: EdgeInsets.symmetric(
                    horizontal: 3,
                    vertical: 4,
                  ),
                  child: Container(
                    height: 100,
                    alignment: Alignment.center,
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 50,
                        child: Padding(
                          padding: EdgeInsets.all(5),
                          child: FittedBox(
                            child: Icon(
                              Icons.category_rounded,
                              color: Theme.of(context).primaryColor,
                              size: 90,
                            ),
                          ),
                        ),
                      ),
                      title: Text(
                        'Others',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text('Dont let anything go to waste.'),
                    ),
                  ),
                ),
                behavior: HitTestBehavior.opaque,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Share With Us'),
        actions: [
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              setState(() {
                if (selectedValue == FilterOptions.Favorite) {
                  _showOnlyFavorite = true;
                } else {
                  _showOnlyFavorite = false;
                }
              });
            },
            icon: Icon(
              Icons.more_vert,
            ),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text("Favorites"),
                value: FilterOptions.Favorite,
              ),
              PopupMenuItem(
                child: Text("Show All"),
                value: FilterOptions.All,
              )
            ],
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: Center(
        child: Container(
          height: deviceSize.height * 0.4,
          width: deviceSize.width * 0.4,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Image(
              image: AssetImage('assets/images/dvlogo.gif'),
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: null,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).accentColor,
        // currentIndex: null,
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.restaurant_menu_rounded),
            label: "Food",
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.category_rounded),
            label: "Non Food",
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _startAddNewTransaction(context),
      ),
    );
  }
}

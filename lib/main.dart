import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './providers/auth.dart';
import './screens/login_screen.dart';
import './screens/main_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Auth(),
        ),
      ],
      child: MaterialApp(
        title: 'Authentication',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          accentColor: Colors.red,
        ),
        home: LoginScreen(),
        routes: {
          MainScreen.routeName: (ctx) => MainScreen(),
        },
      ),
    );
  }
}
